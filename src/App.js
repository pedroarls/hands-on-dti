import './App.css';
import React from 'react'
import {Header} from "./components/Header";
import {TodoList} from "./components/TodoList";

function App() {
    localStorage.clear();

    const QNT_TAREFAS_INCIAL = 10;

    const rangeInicial =[...Array(QNT_TAREFAS_INCIAL).keys()];
    const itensIniciais = rangeInicial.map((item) => {
        return {
            value: item,
            label: `Tarefa ${item}`,
            estaCompleto: item < 2
        }
    })

    const [itens, ] = React.useState(itensIniciais)

    return (
        <div style={{height: '100vh', backgroundColor: "lightgray"}}>
            <Header texto={"TODO LIST"}/>
            <TodoList itens={itens}/>
        </div>

    );
}

export default App;
