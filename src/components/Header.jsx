export const Header = (props) => {
    return(
        <h1 style={{
            display: "flex",
            alignItems: "center",
            justifyContent:"center",
            margin: "10px 0px 0px 10px"
        }}>
            {props.texto}
        </h1>
    )
}