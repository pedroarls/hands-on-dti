import {Checkbox, FormControlLabel} from "@mui/material";
import React from "react";

export const TodoItem = ({value, label, estaCompleto, handleChange}) => {
    const [desabilitado, setDesabilitado] = React.useState(false);

    const eventoCompletarItem = (event) => {
        const valor = event.target.value;
        setDesabilitado(true);
        const agora = new Date();
        localStorage.setItem(valor, agora.toISOString());
        handleChange(valor);
    }

    const horarioConclusao = localStorage.getItem(value);
    const labelComHorario = `${label}  - Data de conclusão: ${horarioConclusao}`;
    const novoLabel = !!horarioConclusao ? labelComHorario : label;

    return(
        <FormControlLabel
            control={<Checkbox/>}
            value={value}
            label={novoLabel}
            onChange={eventoCompletarItem}
            disabled={desabilitado}
        />
    )
}