import React from 'react';
import {TodoItem} from "./TodoItem";

export const TodoList = ({itens}) => {
    const [tarefasConcluidas, ] = React.useState([]);


    const handleConclusaoItem = (value) => {
        tarefasConcluidas.push(value);
    }

    return (
        <div
            style={{
                display: 'flex',
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                marginTop: 10
            }}
        >
            {
                itens.map((item) => {
                    const concluido = tarefasConcluidas.includes(''+item.value);
                    return <TodoItem
                        key={item.value}
                        value={''+item.value}
                        label={item.label}
                        // estaCompleto={!!item.estaCompleto}
                        handleChange={handleConclusaoItem}
                        desabilitado={concluido}
                    />
                })
            }

        </div>
    )
}